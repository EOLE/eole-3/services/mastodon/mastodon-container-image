# -*- coding: utf-8 -*-
# vim: ft=yaml
---
include:
  - project: EOLE/Infra/ci-tools
    ref: stable
    file: /templates/Rules.yaml
  - project: EOLE/Infra/ci-tools
    ref: stable
    file: /templates/Runners/eole-docker.yaml
  - project: EOLE/Infra/ci-tools
    ref: stable
    file: /templates/Git.yaml
  - project: EOLE/Infra/ci-tools
    ref: stable
    file: /templates/Docker.yaml

variables:
  STABLE_BRANCH: main
  IMAGE_NAME: mastodon

stages:
  - lint
  - build
  - release

.rules-map:
  on-release-tag:
    if: $CI_COMMIT_TAG =~ /^release\/[0-9]+\.[0-9]+\.[0-9]+/ && $CI_COMMIT_REF_PROTECTED
    when: on_success


###############################################################################
# `lint` stage: `commitlint`
###############################################################################
commitlint: {extends: '.git:commitlint'}


###############################################################################
# `build` stage: `docker:image`
###############################################################################
# Suffix all jobs to avoid conflict with other jobs names
docker:image:
  extends:
    - .docker:image:build
    - .on-branch


###############################################################################
# `release` stage: `tag *`
###############################################################################
# Create a git tag based on application version
git:tag:create:
  extends:
    - .on-stable
  stage: release
  image: "${GIT_IMAGE}"
  variables:
    GIT_IMAGE: 'bitnami/git:latest'
  script:
    - echo -e "\e[0Ksection_start:$(date +%s):git-tag-create-find-dockerfile[collapsed=true]\r\e[0KFind Dockerfile to use…"
    - |
      test -f Dockerfile.${IMAGE_NAME} && export DOCKERFILE=${DOCKERFILE:-Dockerfile.${IMAGE_NAME}} || true
      test -f Dockerfile && export DOCKERFILE=${DOCKERFILE:-Dockerfile} || true
      echo -e "\e[0Ksection_end:$(date +%s):git-tag-create-find-dockerfile\r\e[0K"
    - echo -e "\e[0Ksection_start:$(date +%s):git-tag-create-configure[collapsed=true]\r\e[0KConfigure git variables from '${CI_COMMIT_AUTHOR}'"
    - |
      export GIT_AUTHOR_NAME="${GIT_AUTHOR_NAME:-${CI_COMMIT_AUTHOR% <*}}"
      export TEMP_MAIL_PREFIX="${CI_COMMIT_AUTHOR#*<}"
      export GIT_AUTHOR_EMAIL="${GIT_AUTHOR_EMAIL:-${TEMP_MAIL_PREFIX%>}}"
      export GIT_COMMITTER_NAME="${GIT_COMMITTER_NAME:-${GIT_AUTHOR_NAME}}"
      export GIT_COMMITTER_EMAIL="${GIT_COMMITTER_EMAIL:-${GIT_AUTHOR_EMAIL}}"
      echo -e "\e[0Ksection_end:$(date +%s):git-tag-create-configure\r\e[0K"
    # Add `upstream` remote to push tag
    # Use `${GITLAB_TOKEN}` for write permission
    - echo -e "\e[0Ksection_start:$(date +%s):git-tag-create-remote-upstream-add[collapsed=true]\r\e[0KAdd upstream repository"
    - |
      git remote show upstream 2> /dev/null || git remote add upstream ${CI_REPOSITORY_URL/${CI_JOB_TOKEN}/${GITLAB_TOKEN}}
      git fetch --all
      echo -e "\e[0Ksection_end:$(date +%s):git-tag-create-remote-upstream-add\r\e[0K"
    - echo -e "\e[0Ksection_start:$(date +%s):git-tag-create-set-new-version[collapsed=true]\r\e[0KCompute new version from '${DOCKERFILE}'"
    - |
      export APP_VERSION=$(sed -nE '/^ARG APP_VERSION=/!{q1} ; s,^ARG APP_VERSION=(.*)$,\1,ip ; q' ${DOCKERFILE} || sed -nE 's,^FROM.*:(.*)$,\1,ip ; q' ${DOCKERFILE})
      export LAST_TAG=$(git tag -l --sort=-version:refname | head -n 1)
      [ -n "${APP_VERSION}" ] || (echo "Can't find APP_VERSION from ${DOCKERFILE}" && exit 1)
    - |
      if [[ "${LAST_TAG}" =~ ${APP_VERSION} ]]
      then
          LAST_INDEX="${LAST_TAG##*.}"
          NEW_TAG="${LAST_TAG%.*}.$(( LAST_INDEX + 1))"
      else
          NEW_TAG=release/${APP_VERSION}-eole3.0
      fi
      echo -e "\e[0Ksection_end:$(date +%s):git-tag-create-set-new-version\r\e[0K"
    - echo -e "\e[0Ksection_start:$(date +%s):git-tag-create-new-tag[collapsed=true]\r\e[0KCreate new git tag '${NEW_TAG}' pointing to '${CI_COMMIT_SHORT_SHA}'"
    - |
      git tag -m "New image release ${NEW_TAG#release/}" ${NEW_TAG}
      git push upstream ${NEW_TAG}
      echo -e "\e[0Ksection_end:$(date +%s):git-tag-create-new-tag\r\e[0K"

## tag contribution branches with a more stable name than `git-${CI_COMMIT_SHORT_SHA}`
docker:tag:contrib-branch:
  extends:
    - .docker:image:tag
    - .on-branches
  variables:
    # `feature/foo-bar_quux` → `feature-foo-bar-quux`
    IMAGE_TAG: $CI_COMMIT_REF_SLUG

docker:tag:release:
  extends: .docker:image:tag

docker:tag:major:
  extends: .docker:image:tag
  before_script:
    - export RELEASE=${CI_COMMIT_TAG#${RELEASE_TAG_PREFIX}}
    - export IMAGE_TAG=${RELEASE%%.*}

docker:tag:minor:
  extends: .docker:image:tag
  before_script:
    - export RELEASE=${CI_COMMIT_TAG#${RELEASE_TAG_PREFIX}}
    - export UPSTREAM_RELEASE=${RELEASE%-*}
    - export IMAGE_TAG=${UPSTREAM_RELEASE%.${UPSTREAM_RELEASE##*.}}

# Follow upstream version without `-eole3.X` suffix
docker:tag:upstream:
  extends: .docker:image:tag
  before_script:
    - export RELEASE=${CI_COMMIT_TAG#${RELEASE_TAG_PREFIX}}
    - export IMAGE_TAG=${RELEASE%-*}

# Follow upstream with `-eole3` suffix
docker:tag:eole:
  extends: .docker:image:tag
  before_script:
    - export RELEASE=${CI_COMMIT_TAG#${RELEASE_TAG_PREFIX}}
    - export IMAGE_TAG=${RELEASE%.*}

docker:tag:stable:
  extends: .docker:image:tag
  variables:
    IMAGE_TAG: stable

docker:tag:latest:
  extends: .docker:image:tag
  variables:
    IMAGE_TAG: latest

# Generate a gitlab release
gitlab:release:
  extends:
    - .on-release-tag
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "Create release from ${CI_COMMIT_TAG} "
  release:
    tag_name: $CI_COMMIT_TAG
    name: "${CI_COMMIT_TAG#${RELEASE_TAG_PREFIX}}"
    description: |
      Download container image <code>${IMAGE_NAME}:${CI_COMMIT_TAG#${RELEASE_TAG_PREFIX}}</code>:

      <code>docker pull ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_TAG#${RELEASE_TAG_PREFIX}}</code>
